# Simuliton

[Audio / Video Equipment](EquipmentLINKS.md)

Simuliton is broadcasting service for `webcast`, `webinars` and `LiveEvents`. Webcasts and webinars are broadcast which is the source of a live data stream, usually a camera and microphone with an internet browser or smartphone. This term is the synonym for the user, network session (a campaign created with simuliton), or function of stream generation.  

 - Webcast is usually passive broadcast for large audience. They're used mostly to increase reach of conferences, sport events or concerts. Popular webcast services are `YouTubeLive`, `FacebookLive`, `StreamShark`. 
 - Webinars are for smaller audience with lots of interaction between participants (chat, poll, whiteboard etc.), and are usually short ~ 1 hour. They're usually low quality streams because of webcams. Popular tools for webinars are `Zoom`, `ClickMeeting`, `Ms Teams`, `Slack`, `GoToWebinar`.  
 - LiveEvents

## Streaming software
 - [OBS](https://obsproject.com/) - Free
- [vMix](https://www.vmix.com/) - Pay
- [Wirecast](https://www.telestream.net/wirecast/) - Pay

## Translator
Simuliton supports translation service (voice transcribe & human voice translation) for joined users aka broadcasters connected with end device.

## CDN (Content Delivery Network)
Simuliton follows CDN approach of spreading the network of servers near user to avoid latency. There is one ORIGIN and n EDGE servers. One of the edge servers stream data near user. End-to-End Latency is the time it takes video or audio captured by a device to go from the publisher to the subscriber.

## Codecs
Simuliton uses audio/video codecs to compress data to smaller format without loosing too much on quality.
 - Audio
    * AAC (Advanced Audio Coding). An audio coding standard for lossy digital audio compression. Designed to be the successor of the MP3 format, AAC generally achieves better sound quality than MP3 at the same bit rate.
 - Video
    * H.264/MPEG-4 AVC (Advanced Video Coding) - A block oriented motion compensation-based video compression codec. It is a standard capable of providing good video quality at substantially lower bit rates than previous standards. It can be implemented in a wide variety of networks and systems and is usable with many protocols.
    * Opus - A lossy audio codec designed to efficiently code speech and general audio in a single format, while remaining low-latency enough for real-time interactive communication and low-complexity enough for low-end embedded processors.
    * Speex - A free software speech codec that may be used on VoIP (Voice Over Internet Protocol) applications. Specifically it is a lossy audio compression format specifically tuned for the reproduction of human speech.

## Bitrate
The number of bits (0 and 1s) that can be transferred or processed from one point to another in a set amount of time (usually one second). Tells codec which quality of stream you want. It's measured in kb/s = kbps (kilo-bit per second), and Mb/s = Mbps (mega-bits per second). It depends on:
 - Bandwidth (Internet speed) - Whether client is connected with cabal, 5G LTE or Wi-Fi. Upload speed must be twice higher of bitrate, meaning if you want 2000 kb/s of bitrate, your upload speed should be 4000 kb/s and above. The total amount of data that can be transferred or processed at any one time (also, usually one second).
 - Resolution of video image
 - Amount of movement during the recording
 
## Protocols
*  RTMP (Real-Time Messaging Protocol) - Developed by Adobe for Flash player and usually used on desktop computers. TCP based protocol for streaming audio, video, and data over the Internet. It allows for low-latency communication. Today RTMP is the most common protocol for publishing streams. Professional encoders, OBS, FFMPEG, etc. all support RTMP.  
    * Audio Codecs: `AAC, AAC-LC, HE-AAC+ v1 & v2, MP3, Speex, Opus, Vorbis`
    * Video Codecs: `H.264, VP8, VP6, Sorenson SparkÂ®, Screen Video v1 & v2`
    * Playback Compatibility: Not widely supported (Flash Player, Adobe AIR, RTMP-compatible players)
    * Benefits: Low-latency and requires no buffering
    * Drawbacks: Not optimized for quality of experience or scalability
    * Latency: 5 seconds
    * Variant Formats: RTMPT (tunneled through HTTP), RTMPE (encrypted), RTMPTE (tunneled and encrypted), RTMPS (encrypted over SSL), RTMFP (travels over UDP instead of TCP)  
*  RTSP/RTP - Like RTMP, RTSP/RTP describes a stateful protocol used for video contribution as opposed to multi-device delivery. While RTMP is a presentation-layer protocol that lets end users command media servers via pause and play capabilities, RTP is a transport protocol used to move said data. Itâ€™s supported by UDP at this same layer.
   Android and iOS devices don't have RTSP compatible players out of the box, making this another protocol thatâ€™s rarely used for playback.
    * Audio Codecs: `AAC, AAC-LC, HE-AAC+ v1 & v2, MP3, Speex, Opus, Vorbis`
    * Video Codecs: `H.265 (preview), H.264, VP9, VP8`
    * Playback Compatibility: Not widely supported (Quicktime Player and other RTSP/RTP-compliant players, VideoLAN VLC media player, 3Gpp-compatible mobile devices)
    * Benefits: Low-latency and requires no buffering
    * Drawbacks: Not optimized for quality of experience and scalability
    * Latency: 2 seconds
    * Variant Formats: The entire stack of RTP, RTCP (Real-Time Control Protocol), and RTSP is often referred to as RTSP
*  HLS (HTTP Live Streaming) - Media streaming communications protocol originally created by Apple that works by breaking the overall stream into a sequence of small HTTP-based file downloads (called chunks).  
   Protocol supports adaptive bitrate streaming, which is key to viewer experience. More importantly, a stream delivered via HLS will play back on the majority of devices â€” thereby expanding your audience.
   While HLS support was initially limited to iOS devices such as iPhones and iPads, native support has since been added to a wide range of platforms. All Google Chrome browsers, as well as Android, Linux, Microsoft, and MacOS devices can play streams delivered using HLS.
    * Audio Codecs: `AAC-LC, HE-AAC+ v1 & v2, MP3`
    * Video Codecs: `H.265, H.264`
    * Playback Compatibility: Great (All Google Chrome browsers; Android, Linux, Microsoft, and MacOS devices; several set-top boxes, smart TVs, and other players)
    * Benefits: Adaptive bitrate and widely supported
    * Drawbacks: Quality of experience is prioritized over low latency
    * Latency: 6-30 seconds (lower latency only possible when tuned)
    * Variant Formats: Low-Latency HLS (see below), PHLS (Protected HTTP Live Streaming)
*  HLS (Usually found on Apple devices)
   Mid 2019, Apple announced an extension to their HLS protocol designed to drive latency down at scale. The protocol achieves this using HTTP/2 PUSH delivery combined with shorter media chunks. Unlike standard HLS, Apple Low-Latency HLS doesnâ€™t yet support adaptive bitrate streaming â€” but it is on the roadmap.  
    * Playback Compatibility: Any players that arenâ€™t optimized for Low-Latency HLS can fall back to standard (higher-latency) HLS behavior
    * Benefits: Low latency meets HTTP-based streaming
    * Drawbacks: As an emerging spec, vendors are still implementing support
    * Latency: 3 seconds or less
*  WebRTC (Web Real-Time Communication) - A universal, plugin-free browser-based communication protocol that enhances browsers with RTC (Real-Time Communication) capabilities via Javascript API's.  
   Users connecting via Chrome, Firefox, or Safari can communicate directly through their browsers â€” enabling sub-500 millisecond latency.    
    * Audio Codecs: `Opus, iSAC, iLBC`
    * Video Codecs: `VP8, VP9`
    * Playback Compatibility: Chrome, Firefox, and Safari support WebRTC without any plugin
    * Benefits: Super fast and browser-based
    * Drawbacks: Designed for video conferencing and not scale
    * Latency: Sub-one-second delivery
*  SRT - This open-source protocol is recognized as a proven alternative to proprietary transport technologies â€” helping to deliver reliable streams, regardless of network quality. From recovering lost packets to preserving timing behavior, SRT was designed to solve the challenges of video contribution and distribution across the public internet. Although itâ€™s quickly taking the industry by storm, SRT is most often used for first-mile contribution rather than last-mile delivery.  
    * Audio Codecs: `Codec-agnostic`
    * Video Codecs: `Codec-agnostic`
    * Playback Compatibility: Limited (currently used for contribution)
    * Benefits: High-quality, low-latency video over suboptimal networks
    * Drawbacks: Playback support is still in the works
    * Latency: 3 seconds or less
*  HDS - Usually found on Android. Developed for use with Flash Player applications as the first adaptive bitrate protocol. Because the end-of-life date for Flash is looming, itâ€™s fallen out of favor.
    * Audio Codecs: `AAC, MP3`
    * Video Codecs: `H.264, VP6`
    * Playback Compatibility: Not widely supported (Flash Player, Adobe AIR)
    * Benefits: Adaptive bitrate technology for Flash
    * Drawbacks: Proprietary technology with lacking support
    * Latency: 6-30 seconds (lower latency only possible when tuned)
*  MPEG-DASH - When it comes to MPEG-DASH, the acronym spells out the story. The Moving Pictures Expert Group (MPEG), an international authority on digital audio and video standards, developed Dynamic Adaptive Streaming over HTTP (DASH) as an industry-standard alternative to HLS. Basically, with DASH you get an open-source option. But because Apple tends to priorities its proprietary software, support for DASH plays second fiddle.  
    * Audio Codecs: `Codec-agnostic`
    * Video Codecs: `Codec-agnostic`
    * Playback Compatibility: Good (All Android devices; most post-2012 Samsung, Philips, Panasonic, and Sony TVs; Chrome, Safari, and Firefox browsers)
    * Benefits: Vendor independent, international standard for adaptive bitrate
    * Drawbacks: Not supported by iOS or Apple TV
    * Latency: 6-30 seconds (lower latency only possible when tuned)
    * Variant Formats: MPEG-DASH CENC (Common Encryption)
*  Low-Latency CMAF for DASH - The Common Media Application Format, or CMAF, is in itself is a media format. But when paired with chunked encoding and chunked transfer encoding for delivery over DASH, it should support sub-three-second delivery. While its transfer setup differs from that of Low-Latency HLS, the use of shorter data segments is quite similar.  
    * Playback Compatibility: Any players that arenâ€™t optimized for low-latency CMAF for DASH can fall back to standard (higher-latency) DASH behavior
    * Benefits: Low latency meets HTTP-based streaming
    * Drawbacks: As an emerging spec, vendors are still implementing support
    * Latency: 3 seconds or less
*  Microsoft Smooth Streaming - For use with Silverlight player applications. It enables adaptive delivery to all Microsoft devices.
    * Audio Codecs: `AAC, MP3, WMA`
    * Video Codecs: `H.264, VC-1`
    * Playback Compatibility: Good (Microsoft and iOS devices, Xbox, many smart TVs)
    * Benefits: Adaptive bitrate and supported by iOS
    * Drawbacks: Proprietary technology
    * Latency: 6-30 seconds (lower latency only possible when tuned)

## Simuliton Web
Simuliton Web uses WebRTC as a communication protocol. When two browsers communicate they establish SDP - Session description protocol. One side contacts signaling server to set local description which becomes remote description. Other side send the same communication message back to establish the browser to browser connection.

![WebRTC](img/webrtc.png)

## Pricing
| License                   | Basic HD    | HD           | 4K                           | PRO                          |
| :---                      |    :----:   |    :----:    |    :----:                    |    :----:                    |
| Price                     | 20$         | 30$      	 | 40$                          | 50$                          |
| Maximum Resolution        | 1920 x 1080 | 1920 x 1080	 | 4096 x 2160                  | 4096 x 2160                  |
| Streaming                 | Yes         | Yes          | Yes (2 simultaneous streams) | Yes (4 simultaneous streams) |
| Recording                 | No          | Yes (<500Mb) | Yes (<10Tb)                  | Yes (<10Tb)                  |
| Chat support              | No          | Yes          | Yes                          | Yes                          |
| Auto transcribe with CC   | No          | No           | Yes                          | Yes                          |