### Links to hardware and software recommended by me for streaming

**Webcams:**

In my opinion, you should look ONLY for top-shelf webcams and only if you don't have money for anything better (even a cheap handycam is better for live streaming than webcam for most situations if you care about the quality):

*   Facetiime webcams built-in modern MacBook Pros and iMacs.

*   [Logitech C922](https://www.logitech.com/en-us/product/c922-pro-stream-webcam)

**Actions cams:**

1.  [GoPro 4 Silver](https://www.bhphotovideo.com/c/product/1078007-REG/gopro_chdhy_401_hero4_silver_edition_adventure.html) or Black (in my opinion SIlver is better than Black for live streaming because it's cheaper and it lasts longer on a batteries. Black is useful if you want to shoot in 4K but for live streaming it's not necessary). I habe on GoPro 4 Silver in my own equipment as a backup cam.

2.  [YI Technology 4K Action Camera](https://www.bhphotovideo.com/c/product/1255908-REG/yi_technology_yac4kb_4k_action_camera_black.html) - It offers simmilar quality than GoPro 4 but for much less price and lasts much longer on a battery. I didn't play with it yet but it's worth a look.

3.  [Sony FDR-X1000V](https://www.bhphotovideo.com/c/product/1111159-REG/sony_fdr_x1000v_r_fdr_x1000v_4k_action_camera.html) - Main competitor for GoPro 4, similar price. To be honest I don't know whether it offers clean HDMI output required for live streaming or not, please let us all know in discussion group of this course if you have any experience with this cam.

**Handycams:**

A few years ago I was using Sony HDR-XR500 in it was a great cam for the start! I really loved quality of the picture and simplicity of this cam. That's why I'm a "fan boy" of Sony but there are also great handycams from Panasonic, Canon and JVC.

_Remember to choose a cam with mini-jack mic input! (not all the handycams have one)._

1.  [Canon VIXIA HF G40](https://www.bhphotovideo.com/c/product/1210608-REG/canon_1005c002_vixia_hf_g40_full.html)

2.  [Panasonic HC-V770K](https://www.bhphotovideo.com/c/product/1109405-REG/panasonic_hc_v770_full_hd_camcorder.html)

3.  [Sony FDR-AX53 4K ](https://www.bhphotovideo.com/c/product/1211905-REG/sony_fdrax53_b_fdr_ax53_4k_ultra_hd.html)

4.  [Panasonic HC-X920 ](https://www.bhphotovideo.com/c/product/910265-REG/panasonic_hc_x920k_hc_x920_3mos_ultrafine_full.html)

5.  [Sony FDR-AX100](https://www.bhphotovideo.com/c/product/1022653-REG/sony_fdrax100_b_hdr_ax100_full_hd_handycam.html)

6.  [Canon VIXIA HF G40](https://www.bhphotovideo.com/c/product/1210608-REG/canon_1005c002_vixia_hf_g40_full.html)

**DSLRs and Mirrorless:**

SUPER-IMPORTANT: You can use any type of DSLR or Mirrorless but it has to have CLEAN HDMI output!!! (See lesson about DSLRs and Mirrorless for more explanation):

*   [Panasonic DMC-GH4R](http://cvp.com/index.php?t=product/panasonic_dmc-gh4r) - currently my main cam, I LOVE IT. But you have to look for "R" version - it doesn't have stupid, 30-minutes per recorded clip limitation. You can buy even [this interface unit](https://www.bhphotovideo.com/c/product/1028546-REG/panasonic_dmc_gh4_yagh_lumix_yagh_interface_unit.html) that provides GH4 with two pro XLR inputs and pro SDI outputs (!). I think it's the best.

*   [Sony A7SII](https://www.bhphotovideo.com/c/product/1186034-REG/sony_ilce7sm2_b_alpha_a7sii_mirrorless_digital.html)- amazing cam if you are looking for something that performs better in low light. However, it overheats sometimes, please read previous carefully before buying anything or rent one for testing. 

*   [Canon 5D Mark III](https://www.bhphotovideo.com/c/product/847545-REG/Canon_5260A002_EOS_5D_Mark_III.html) - all-time classic with clean HDMI-output but limits recordings to 30 minutes (you can check [http://www.magiclantern.fm](http://www.magiclantern.fm/) that helps overcome this issue). There is also a new version called [Canon 5D Mark IV](https://www.bhphotovideo.com/c/product/1274705-REG/canon_eos_5d_mark_iv.html) but it doesn't have very good revious from video producers. For similar price you can buy[Canon C100](https://www.bhphotovideo.com/c/product/1086125-REG/canon_eos_c100_cinema_eos.html) which is sooooo much better for live streaming (no video recording limitation, pro audio inputs and better ergonomics). 

*   [Nikon D500 DSLR](https://www.bhphotovideo.com/c/product/1214161-REG/nikon_1559_d500_dslr_camera_body.html)  - similar to Canon 5D, if don't like Canon ;)

*   [Mirrorless Cameras: A Buying Guide](http://www.bhphotovideo.com/explora/photography/buying-guide/mirrorless-cameras)

*   [DSLR vs. Mirrorless Cameras: Which Is Better for You?](http://www.tomsguide.com/us/dslr-vs-mirrorless-cameras,news-17736.html)

**Pro Cams:**

*   [Sony PXW-70](https://www.bhphotovideo.com/c/product/1072752-REG/sony_pxw_x70_professional_xdcam_compact.html) - probably the best cam for live streaming for 2000$ (in Filmpoint we have 4 of them!). It has SDI output, great image quality (4K sensor!) and nice zoom.

*   [Canon C100](https://www.bhphotovideo.com/c/product/1086125-REG/canon_eos_c100_cinema_eos.html) - if you have 4000$ and looking for something more "cinema-like" not only for streaming but also for commercials and promo videos.

*   [Sony FS700](https://www.bhphotovideo.com/c/product/1010153-REG/sony_nex_fs700r_4k_nxcam_super_35mm.html)- similar to Canon C100

*   [Blackmagic Design Micro Studio Camera](https://www.bhphotovideo.com/c/product/1137295-REG/blackmagic_design_cinstudmft_uhd_mr_micro_studio_cinema_camera.html) - crazy invention by Blackmagic, worth a look if you are using Blackmagic ATEM switchers (you can remotely adjust many settings like focus and zoom with those switchers). 

*   [Blackmagic Design Studio Camera 4K](https://www.bhphotovideo.com/c/product/1044789-REG/blackmagic_design_cinstudmft_uhd_studio_camera_4k.html) - bigger and more powerful version.  

**HDMI-SDI converters:**

*   [Atomos H2S](https://www.bhphotovideo.com/c/product/964935-REG/atomos_atomh2s002_1xh2s_converter_1_2600mah.html)

*   [Datavideo DAC70](https://www.bhphotovideo.com/c/product/1016990-REG/datavideo_dac_70_up_down_cross_converter_supports.html) - this one is really good.

**USB mics:**

*   [Blue Yeti](https://www.bhphotovideo.com/c/product/1103930-REG/blue_836213002070_yeti_usb_microphone_black.html) - probably the best value for money.

*   [Blue Snowball](https://www.bhphotovideo.com/c/product/1252289-REG/blue_snowball_ice_black_snowball_usb_condenser_microphone.html)

*   [Rode Podcaster](https://www.bhphotovideo.com/c/product/450171-REG/Rode_PODCASTER_Podcaster_USB_Broadcast_Microphone.html)

*   [Podcasters' Buying Guide](http://www.bhphotovideo.com/explora/audio/buying-guide/podcasters-buying-guide)

**On-camera mics and adapters:**

*   [Rode VideoMic GO](https://www.bhphotovideo.com/c/product/1012003-REG/rode_videomic_go_videomic_go_on_camera_shotgun.html)

*   [Rode VideoMic Pro](https://www.bhphotovideo.com/c/product/1152351-REG/rode_videomic_pro_r_videomic_pro_with_lyre.html)

*   [Beachtek MCC-2](https://www.bhphotovideo.com/c/product/962864-REG/beachtek_mcc_2_2_channel_audio_adapter.html)

*   [Beachtek DXA-SLR ULTRA](https://www.bhphotovideo.com/c/product/1038386-REG/beachtek_dxa_slr_ultra_dxa_slr_active_dslr_adapter.html)

*   [On-Camera Shotgun Microphones, A to Z](http://www.bhphotovideo.com/explora/audio/buying-guide/shotgun-microphones)

*   [THE DEFINITIVE GUIDE TO AUDIO INPUT OPTIONS FOR THE PANASONIC GH4](https://suggestionofmotion.com/blog/panasonic-gh4-audio-input-options/)

**Lavalier mics (wireless):**

*   [Sennheiser AVX MKE2](https://www.bhphotovideo.com/c/product/1135452-REG/sennheiser_avx_mke2_set_4_us_avx_camera_mountable_lavalier_pro.html)

*   [Sony UWP-D11](https://www.bhphotovideo.com/c/product/1025751-REG/sony_uwp_d11_30_bodypack_lavalier_mic_eng_package.html)

**DIs**

*   [Radial Engineering ProDI Passive](https://www.bhphotovideo.com/c/product/384382-REG/Radial_Engineering_R800_1100_ProDI_Direct_Box.html) (or others - ask in local store for musicians)

**Internal capture cards**

Before buying, please read compatibility manuals on vMix or Wirecast websites.

*   [Blackmagic Design DeckLink Mini Recorder ](https://www.bhphotovideo.com/c/product/964122-REG/blackmagic_design_decklink_mini_recorder.html)

*   [Blackmagic Design DeckLink Duo 2](https://www.bhphotovideo.com/c/product/1247779-REG/blackmagic_design_bdlkduo2_decklink_duo_2.html)

*   [Blackmagic Design DeckLink Quad](https://www.bhphotovideo.com/c/product/1186263-REG/blackmagic_design_bdlkdvqd2_decklink_quad_2_8_channel.html)

**External capture cards**

*   [Magewell XI100DUSB SDI USB 3.0](https://www.bhphotovideo.com/c/product/1105736-REG/magewell_xi_100_d_usb_sdi_xi100_sdi_usb_3_0.html) and [Magewell XI100DUSB HDMI US 3.0](https://www.bhphotovideo.com/c/product/1105735-REG/magewell_xi_100_d_usb_hdmi_one_hd_hdmi_usb.html) - the best dongles I worked with so far.

*   [Blackmagic Design UltraStudio Mini Recorder](https://www.bhphotovideo.com/c/product/892453-REG/Blackmagic_Design_bdlkulsdzminrec_Ultrastudio_Mini_Recorder.html)

*   [Blackmagic Design UltraStudio Express](https://www.bhphotovideo.com/c/product/857462-REG/Blackmagic_Design_BDLKULSDEXPRESS_UltraStudio_Express.html)

**Software video mixers and encoders**

*   [http://www.vmix.com/](http://http://www.vmix.com/)

*   [http://telestream.net/wirecast](http://telestream.net/wirecast)

*   [https://obsproject.com/index](https://obsproject.com/index)

**Hardware video mixers (single-format)**

*   Blackmagic ATEM Mini Pro

*   [Roland V-1HD ](https://www.bhphotovideo.com/c/product/1198625-REG/roland_v_1hd_4_x_hdmi.html)

*   [Blackmagic Design ATEM Production Studio](https://www.bhphotovideo.com/c/product/1137285-REG/blackmagic_design_swatempsw04k_atem_production_studio_4k.html)

*   [Blackmagic Design ATEM 1 M/E Production Studio](https://www.bhphotovideo.com/c/product/1007191-REG/blackmagic_design_swatempsw1me4k_atem_1_m_e_production.html)

**Hardware video mixers (multi-format)**

*   [Roland V-40HD](https://www.bhphotovideo.com/c/product/900807-REG/Roland_V_40HD_Multi_Format_Video_Switcher.html)

*   [Roland VR-50HD](https://www.bhphotovideo.com/c/product/983417-REG/roland_vr_50hd_multi_format_av_mixer.html)

**Hardware + software video mixers (all-in-one)**

*   [NewTek TriCaster Mini HD-4](https://www.bhphotovideo.com/c/product/1085182-REG/newtek_fg_000874_r001_tricaster_mini_hd_4.html)

*   [NewTek TriCaster 410](https://www.bhphotovideo.com/c/product/1030737-REG/newtek_fg_000716_r001_tricaster_410.html)

*   [vMix GO](http://www.vmix.com/products/vmix-go.aspx)

*   [Wirecast Gear](http://www.telestream.net/wirecast/wirecast-gear.htm)

*   [Custom streaming PCs by Tom Sinclar](http://easternshorebroadcasting.com/hardware2/) - I didn't mention it in the course but there is one more (and really cost effective) solution if you want all-in-one switcher - You can ask Tom Sinclar who is building custom streaming PCs for help. It should be at least 20% cheaper than Wirecast Gear of vMix GO but similar in features or even better. You should check this out. 

**Platforms for webcasts (paid, pro white-label)**

*   [https://streamshark.io](https://streamshark.io/)  -my favourite platform

*   [https://vimeo.com/live](https://vimeo.com/live)

*   [https://cloud.wowza.com](https://cloud.wowza.com) - use it as a backup for StreamShark

*   [http://dacast.com](http://dacast.com) - I prefer StreamShark than Dacast but Dacast is also worth a look

*   [http://cleeng.com](http://cleeng.com/) - if you want to sell access to your live streams!

**Platforms for webcasts (free, ads)**

*   [Facebook LIVE](https://live.fb.com/)

*   [YouTube LIVE](https://www.youtube.com/)

*   [Twitch](https://www.twitch.tv/)

**Platforms for webinars**

*   [https://livestorm.co](https://livestorm.co) - new player on the market. Looks very promising (direct competitor to WebinarNinja, GoToWebinar and other all-in-solutions).

*   [https://streamshark.io](https://streamshark.io) (check lesson about platforms for webcasts and webinars to learn why I choose StreamShark in most of situations even for webinars together with a few other tools that I integrate)

*   [http://zoom.us](http://zoom.us)

*   [https://clickmeeting.com](https://clickmeeting.com) (with Magewell XI100 you should connect pro cam to it but it requires testing, I rather use Zoom.us for video conferencing)

*   [https://www.gotomeeting.com/webinar](https://www.gotomeeting.com/webinar) (with Magewell XI100 you should connect pro cam to it but it requires testing, I rather use Zoom.us for video conferencing)

*   [https://webinarninja.co](https://webinarninja.co) (with Magewell XI100 you should connect pro cam to it but it requires testing, I rather use Zoom.us for video conferencing)

**Other materials:**

*   [How to encode source video for Wowza Streaming Cloud](https://www.wowza.com/forums/content.php?681-How-to-encode-source-video-for-Wowza-Streaming-Cloud#calculate)
