package com.simuliton;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SimulitonSignalingServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(SimulitonSignalingServerApplication.class, args);
	}

}
