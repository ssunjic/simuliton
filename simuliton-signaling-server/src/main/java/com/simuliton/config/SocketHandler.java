package com.simuliton.config;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import org.springframework.stereotype.Component;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.TextWebSocketHandler;

/**
 * Create a message handler to process the WebSocket messages that we'll receive
 * from multiple clients. This is essential to aid the exchange of metadata between
 * the different clients to establish a direct WebRTC connection.
 * Here, to keep things simple, when we receive the message from a client,
 * we will send it to all other clients except to itself.
 * To do this, we can extend TextWebSocketHandler from the Spring WebSocket
 * library and override both the handleTextMessage and afterConnectionEstablished methods:
 */
@Component
public class SocketHandler extends TextWebSocketHandler {

    List<WebSocketSession> sessions = new CopyOnWriteArrayList<>();

    @Override
    public void handleTextMessage(WebSocketSession session, TextMessage message)
            throws IOException {
        for (WebSocketSession webSocketSession : sessions) {
            if (webSocketSession.isOpen() && !session.getId().equals(webSocketSession.getId())) {
                webSocketSession.sendMessage(message);
            }
        }
    }

    @Override
    public void afterConnectionEstablished(WebSocketSession session) throws Exception {
        sessions.add(session);
    }
}
